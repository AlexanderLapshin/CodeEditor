﻿namespace CodeEditor.Logic.Models
{
    public class CaretPosition
    {
        public int Column { get; set; }
        public int Row { get; set; }
    }
}
