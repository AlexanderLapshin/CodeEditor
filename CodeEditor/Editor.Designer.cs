﻿namespace CodeEditor
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editor));
            this.CodeEditor = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createConstantCtrShiftKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decomposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractMethodCtrShiftEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameMethodCtrShiftRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.labelSaved = new System.Windows.Forms.Label();
            this.panelRefactoringMenu = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.textBoxNewname = new System.Windows.Forms.TextBox();
            this.textBoxSelection = new System.Windows.Forms.TextBox();
            this.labelNewname = new System.Windows.Forms.Label();
            this.labelSelection = new System.Windows.Forms.Label();
            this.labelOperation = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panelRefactoringMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // CodeEditor
            // 
            this.CodeEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeEditor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(41)))), ((int)(((byte)(35)))));
            this.CodeEditor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeEditor.Font = new System.Drawing.Font("Consolas", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CodeEditor.ForeColor = System.Drawing.Color.White;
            this.CodeEditor.Location = new System.Drawing.Point(0, 22);
            this.CodeEditor.Name = "CodeEditor";
            this.CodeEditor.Size = new System.Drawing.Size(1184, 639);
            this.CodeEditor.TabIndex = 0;
            this.CodeEditor.Text = "";
            this.CodeEditor.SelectionChanged += new System.EventHandler(this.CodeEditor_SelectionChanged);
            this.CodeEditor.TextChanged += new System.EventHandler(this.CodeEditor_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(5, 665);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Line:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.findToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFIleToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.saveFileToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newFIleToolStripMenuItem
            // 
            this.newFIleToolStripMenuItem.Name = "newFIleToolStripMenuItem";
            this.newFIleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFIleToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.newFIleToolStripMenuItem.Text = "New FIle";
            this.newFIleToolStripMenuItem.Click += new System.EventHandler(this.newFIleToolStripMenuItem_Click);
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.saveFileToolStripMenuItem.Text = "Save File";
            this.saveFileToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createConstantCtrShiftKToolStripMenuItem,
            this.decomposeToolStripMenuItem,
            this.extractMethodCtrShiftEToolStripMenuItem,
            this.renameMethodCtrShiftRToolStripMenuItem,
            this.renameVariableToolStripMenuItem});
            this.findToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.findToolStripMenuItem.Text = "Find";
            // 
            // createConstantCtrShiftKToolStripMenuItem
            // 
            this.createConstantCtrShiftKToolStripMenuItem.Name = "createConstantCtrShiftKToolStripMenuItem";
            this.createConstantCtrShiftKToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.C)));
            this.createConstantCtrShiftKToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.createConstantCtrShiftKToolStripMenuItem.Text = "Create Constant";
            this.createConstantCtrShiftKToolStripMenuItem.Click += new System.EventHandler(this.MenuItemCreateConstant_Click);
            // 
            // decomposeToolStripMenuItem
            // 
            this.decomposeToolStripMenuItem.Name = "decomposeToolStripMenuItem";
            this.decomposeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.D)));
            this.decomposeToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.decomposeToolStripMenuItem.Text = "Decompose";
            this.decomposeToolStripMenuItem.Click += new System.EventHandler(this.MenuItemDecompose_Click);
            // 
            // extractMethodCtrShiftEToolStripMenuItem
            // 
            this.extractMethodCtrShiftEToolStripMenuItem.Name = "extractMethodCtrShiftEToolStripMenuItem";
            this.extractMethodCtrShiftEToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.E)));
            this.extractMethodCtrShiftEToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.extractMethodCtrShiftEToolStripMenuItem.Text = "Extract Method";
            this.extractMethodCtrShiftEToolStripMenuItem.Click += new System.EventHandler(this.MenuItemExtractMethod_Click);
            // 
            // renameMethodCtrShiftRToolStripMenuItem
            // 
            this.renameMethodCtrShiftRToolStripMenuItem.Name = "renameMethodCtrShiftRToolStripMenuItem";
            this.renameMethodCtrShiftRToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.R)));
            this.renameMethodCtrShiftRToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.renameMethodCtrShiftRToolStripMenuItem.Text = "Rename Method";
            this.renameMethodCtrShiftRToolStripMenuItem.Click += new System.EventHandler(this.MenuItemRenameMethod_Click);
            // 
            // renameVariableToolStripMenuItem
            // 
            this.renameVariableToolStripMenuItem.Name = "renameVariableToolStripMenuItem";
            this.renameVariableToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
            | System.Windows.Forms.Keys.V)));
            this.renameVariableToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.renameVariableToolStripMenuItem.Text = "Rename Variable";
            this.renameVariableToolStripMenuItem.Click += new System.EventHandler(this.MenuItemRenameVariable_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // labelSaved
            // 
            this.labelSaved.AutoSize = true;
            this.labelSaved.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSaved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelSaved.Location = new System.Drawing.Point(1126, 665);
            this.labelSaved.Name = "labelSaved";
            this.labelSaved.Size = new System.Drawing.Size(43, 13);
            this.labelSaved.TabIndex = 3;
            this.labelSaved.Text = "Saved";
            this.labelSaved.Visible = false;
            // 
            // panelRefactoringMenu
            // 
            this.panelRefactoringMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRefactoringMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.panelRefactoringMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRefactoringMenu.Controls.Add(this.buttonCancel);
            this.panelRefactoringMenu.Controls.Add(this.buttonApply);
            this.panelRefactoringMenu.Controls.Add(this.textBoxNewname);
            this.panelRefactoringMenu.Controls.Add(this.textBoxSelection);
            this.panelRefactoringMenu.Controls.Add(this.labelNewname);
            this.panelRefactoringMenu.Controls.Add(this.labelSelection);
            this.panelRefactoringMenu.Controls.Add(this.labelOperation);
            this.panelRefactoringMenu.Location = new System.Drawing.Point(873, 22);
            this.panelRefactoringMenu.Name = "panelRefactoringMenu";
            this.panelRefactoringMenu.Size = new System.Drawing.Size(296, 141);
            this.panelRefactoringMenu.TabIndex = 4;
            this.panelRefactoringMenu.Visible = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(50)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCancel.Location = new System.Drawing.Point(197, 105);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(50)))));
            this.buttonApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonApply.Location = new System.Drawing.Point(31, 105);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 5;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = false;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // textBoxNewname
            // 
            this.textBoxNewname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNewname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxNewname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxNewname.Location = new System.Drawing.Point(102, 71);
            this.textBoxNewname.Name = "textBoxNewname";
            this.textBoxNewname.Size = new System.Drawing.Size(170, 20);
            this.textBoxNewname.TabIndex = 4;
            // 
            // textBoxSelection
            // 
            this.textBoxSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSelection.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxSelection.Location = new System.Drawing.Point(102, 35);
            this.textBoxSelection.Name = "textBoxSelection";
            this.textBoxSelection.Size = new System.Drawing.Size(170, 20);
            this.textBoxSelection.TabIndex = 3;
            // 
            // labelNewname
            // 
            this.labelNewname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNewname.AutoSize = true;
            this.labelNewname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNewname.Location = new System.Drawing.Point(28, 74);
            this.labelNewname.Name = "labelNewname";
            this.labelNewname.Size = new System.Drawing.Size(72, 13);
            this.labelNewname.TabIndex = 2;
            this.labelNewname.Text = "New Name:";
            // 
            // labelSelection
            // 
            this.labelSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSelection.AutoSize = true;
            this.labelSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelection.Location = new System.Drawing.Point(28, 38);
            this.labelSelection.Name = "labelSelection";
            this.labelSelection.Size = new System.Drawing.Size(64, 13);
            this.labelSelection.TabIndex = 1;
            this.labelSelection.Text = "Selection:";
            // 
            // labelOperation
            // 
            this.labelOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOperation.AutoSize = true;
            this.labelOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOperation.Location = new System.Drawing.Point(28, 7);
            this.labelOperation.Name = "labelOperation";
            this.labelOperation.Size = new System.Drawing.Size(41, 13);
            this.labelOperation.TabIndex = 0;
            this.labelOperation.Text = "label2";
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(177)))), ((int)(((byte)(186)))));
            this.ClientSize = new System.Drawing.Size(1184, 681);
            this.Controls.Add(this.panelRefactoringMenu);
            this.Controls.Add(this.labelSaved);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CodeEditor);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Editor";
            this.Text = "XLine Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editor_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelRefactoringMenu.ResumeLayout(false);
            this.panelRefactoringMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox CodeEditor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createConstantCtrShiftKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extractMethodCtrShiftEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameMethodCtrShiftRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label labelSaved;
        private System.Windows.Forms.Panel panelRefactoringMenu;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.TextBox textBoxNewname;
        private System.Windows.Forms.TextBox textBoxSelection;
        private System.Windows.Forms.Label labelNewname;
        private System.Windows.Forms.Label labelSelection;
        private System.Windows.Forms.Label labelOperation;
        private System.Windows.Forms.ToolStripMenuItem renameVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decomposeToolStripMenuItem;
    }
}
