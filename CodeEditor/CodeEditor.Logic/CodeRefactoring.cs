﻿using System;
using System.Text.RegularExpressions;

namespace CodeEditor.Logic
{
    public static class CodeRefactoring
    {
        public static string CreateConstant(string file, int selectionStart, int selectionEnd, string newName)
        {
            if (file == null || selectionEnd == 0 || newName == null)
            {
                return null;
            }
            int j = 0;
            for (int i = selectionEnd; i > 0; i--)
            {
                if (file[i] == '"') j++;
                if (file[i].ToString() == "\n") break;
            }
            if (j % 2 != 0)
            {
                return file;
            }
            string copy = file;
            copy = file.Substring(selectionStart, selectionEnd - selectionStart);
            j = -1;
            int pos = 0;
            while (pos != -1)
            {
                pos = file.IndexOf(copy, j + 1);
                j = pos;
                if (pos != -1 && file[pos - 1] != '-' && char.IsDigit(file[pos - 1]) != true && char.IsDigit(file[pos + copy.Length]) != true)
                {
                    file = file.Remove(pos, copy.Length);
                    file = file.Insert(pos, newName);
                }
            }
            copy = "const " + newName + " = " + copy + ";\n"; // создание самой константы              
            file = copy + file;
            return file;
        }

        public static string ExtractMethod(string file, int selectionStart, int selectionEnd, string newName)
        {
            var len = file.Length;
            //var findMethodRegex = new Regex(@"^(?!(for|while|if|else if|switch)(\s*\())[_a-zA-Z0-9]*\s*[_a-zA-Z][_a-zA-Z0-9]*\s*\(.*\)");
            var selectedText = file.Substring(selectionStart, selectionEnd - selectionStart);

            try
            {
                // if (!findMethodRegex.IsMatch(selectedText))
                //{
                string callNewMentodText = $"{newName}();";
                file = file.Replace(selectedText, callNewMentodText);

                string newMethodImplementation = $"\nvoid {newName}()\n" + "{" + $"\n{selectedText}\n" + "}";
                file += newMethodImplementation;

                return file;
                //}
            }
            catch { }

            return file;
        }

        public static string RenameMethod(string file, string newName, int selectionStart, int selectionEnd)
        {
            string CurrentName = file.Substring(selectionStart, selectionEnd - selectionStart);
            int newLineIndex = file.Substring(selectionStart).IndexOf("\n");
            string selectionToEndLine = newLineIndex > 0 ? file.Substring(selectionStart, newLineIndex) : file.Substring(selectionStart);
            var findCycleRegex = new Regex(@"^for|while|if|else if|switch");

            if (!findCycleRegex.IsMatch(CurrentName))
            {
                try
                {
                    var findMethodRegex = new Regex($@"{CurrentName}\s*\(.*\)");
                    if (findMethodRegex.IsMatch(selectionToEndLine))
                    {
                        string pattern = $@"({CurrentName}\s*)" + @"(\()";
                        file = Regex.Replace(file, pattern, newName + "$2");
                        return file;
                    }
                }
                catch { }
            }
            return file;
        }

        public static string RenameVariable(string source, string prevVar, string newVar)
        {
            string b = " ", e = " ", str;
            string pattern = @"\b" + prevVar + @"\b";
            int strLocation;

            string b2, str2;
            if (source.IndexOf('/') >= 0)
            {
                b = source.Substring(0, source.IndexOf('/'));
                e = source.Substring(source.IndexOf('/'));
                if (e.IndexOf('\n') < 0)
                {
                    e = e + "\n";
                }
                strLocation = e.IndexOf('\n');
                str = e.Substring(0, strLocation);
                e = e.Replace(str, String.Empty);
                b = Regex.Replace(b, pattern, newVar);
                b = b + str;

                while (e.IndexOf('/') >= 0 && e != "/")
                {
                    b2 = e.Substring(0, e.IndexOf('/'));
                    e = e.Substring(e.IndexOf('/'));
                    if (e.IndexOf('\n') > 0)
                        str2 = e.Substring(0, e.IndexOf('\n'));
                    else str2 = "";
                    if (e != "/")
                        e = e.Substring(e.IndexOf('\n'));
                    b2 = Regex.Replace(b2, pattern, newVar);
                    b = b + b2 + str2;
                }

                e = Regex.Replace(e, pattern, newVar);
                return b + e;
            }
            else
            {
                b = Regex.Replace(source, pattern, newVar);
                return b;
            }
        }

        public static string Decompose(string source, int num, string name)
        {
            string b, e, temp;
            temp = e = source;
            int charLocation = 0;

            for (int i = 0; i < num - 1; i++)
            {
                charLocation = e.IndexOf('\n');
                e = e.Substring(charLocation + 1);
            }

            charLocation = e.IndexOf('\n');
            e = e.Substring(0, charLocation);
            e = " " + e;
            e = Regex.Replace(e, "\t", " ");
            e = Regex.Replace(e, "\\s+", " ");


            if (source.IndexOf(name) > 0)
                return source;

            if (e.IndexOf(" if") == 0)
            {
                e = e.Remove(0, e.IndexOf("(") + 1);
                if (e.IndexOf("{") >= 0)
                {
                    e = e.Substring(0, e.IndexOf("{"));
                }
                e = e.Replace(") ", ")");
                e = e.Remove(e.Length - 1, 1);

                string name2 = name;
                name2 = name2.Replace("*", String.Empty);
                name2 = Regex.Replace(name2, @"(\w+?)\s", String.Empty);

                if (name2.IndexOf("::") >= 0)
                {
                    name2 = Regex.Replace(name2, @"(\w+?)::", String.Empty);
                }
                temp = temp.Replace(e, name2);

                if (name2 == e)
                    return source;

                b = "bool " + name +
                    "\r\n" + "{\r\n       return " + e + ";\r\n" + "}\r\n";

                if (source.IndexOf("using namespace std;") >= 0)
                {
                    temp = temp.Replace("using namespace std;", "using namespace std;\r\n" + b);
                }
                else
                {
                    temp = b + temp;
                }
                return temp;
            }
            else
            {
                return source;
            }
        }
    }
}
