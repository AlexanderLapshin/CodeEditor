﻿using CodeEditor.Logic;
using FluentAssertions;
using Xunit;

namespace CodeEditor.Tests
{
    public class CreateConstantTests
    {

        [Fact]
        public void numberPiToConstant()
        {
            // Arrange
            string file = "double s = 3.14 + 2;";
            int selectionStart = 11;
            int selectionEnd = 15;
            string newName = "Pi";
            var expected = "const Pi = 3.14;\ndouble s = Pi + 2;";

            // Act     
            // Create a mat. constant  pi
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void numberZeroToConstant()
        {
            // Arrange
            string file = "int s = 99;\nint res = 7 + 0;";
            int selectionStart = 26;
            int selectionEnd = 27;
            string newName = "Zero";
            var expected = "const Zero = 0;\nint s = 99;\nint res = 7 + Zero;";

            // Act
            // Create a mat. constant zero 
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void number_e_ToConstant()
        {
            // Arrange
            string file = "int s = 99;\ndouble res = 7 + 2.7;\ndouble d = 99.0;";
            int selectionStart = 29;
            int selectionEnd = 32;
            string newName = "E";
            var expected = "const E = 2.7;\nint s = 99;\ndouble res = 7 + E;\ndouble d = 99.0;";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void number_50_ToConstant()
        {
            // Arrange
            string file = "double res = 7 + 50;\ndouble d = 99.0;";
            int selectionStart = 17;
            int selectionEnd = 19;
            string newName = "Fifty";
            var expected = "const Fifty = 50;\ndouble res = 7 + Fifty;\ndouble d = 99.0;";
            // Act
            // Create a mat. constant Fifty=50
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void TestErrorAllNull()
        {         
            // Act           
            var result = CodeRefactoring.CreateConstant(null, 0, 0, null);
            // Assert
            result.Should().Be(null);
        }

        [Fact]
        public void TestErrorTwoNull()
        {           
            // Act           
            var result = CodeRefactoring.CreateConstant(null, 8, 11, null);
            // Assert
            result.Should().Be(null);
        }

        [Fact]
        public void NumberTenInString()
        {
            // Arrange
            string file = "string x = \"10 + 3\";";
            int selectionStart = 13;
            int selectionEnd = 15;
            string newName = "Ten";
            var expected = "string x = \"10 + 3\";";
            // Act   
            // Create a mat. constant Ten=10
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void cw_e_ToConstant()
        {
            // Arrange
            string file = "int s = 99;\nConsole.WriteLine(2.7);\ndouble d = 99.0;";
            int selectionStart = 30;
            int selectionEnd = 33;
            string newName = "E";
            var expected = "const E = 2.7;\nint s = 99;\nConsole.WriteLine(E);\ndouble d = 99.0;";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void Several_e_ToConstant()
        {
            // Arrange
            string file = "double x = 2.7 + 2.7;";
            int selectionStart = 11;
            int selectionEnd = 14;
            string newName = "E";
            var expected = "const E = 2.7;\ndouble x = E + E;";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void checkNumber()
        {
            // Arrange
            string file = "double res = 7 + 50;\ndouble d = 5050;";
            int selectionStart = 17;
            int selectionEnd = 19;
            string newName = "Fifty";
            var expected = "const Fifty = 50;\ndouble res = 7 + Fifty;\ndouble d = 5050;";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void checkMinus()
        {
            // Arrange
            string file = "double x = 2.7 + -2.7;";
            int selectionStart = 11;
            int selectionEnd = 14;
            string newName = "E";
            var expected = "const E = 2.7;\ndouble x = E + -2.7;";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void checkInFunction()
        {
            // Arrange
            string file =
                "for (int i = 0; i < row; i++)\n" +
                "{\n" +
                  "matrix[i][0] = rand() % 3 + 1;\n" +
                    "matrix[i][1] = rand() % 100 + 100;\n" +
                "}\n" +
                "Sleep(0);\n" +
                "LeaveCriticalSection(&critsect);\n";
            int selectionStart = 13;
            int selectionEnd = 14;
            string newName = "Zero";
            var expected =
                "const Zero = 0;\n" +
                "for (int i = Zero; i < row; i++)\n" +
                "{\n" +
                  "matrix[i][Zero] = rand() % 3 + 1;\n" +
                    "matrix[i][1] = rand() % 100 + 100;\n" +
                "}\n" +
                "Sleep(Zero);\n" +
                "LeaveCriticalSection(&critsect);\n";
            // Act
            // Create a mat. constant e
            var result = CodeRefactoring.CreateConstant(file, selectionStart, selectionEnd, newName);

            // Assert
            result.Should().Be(expected);
        }
    }
}
