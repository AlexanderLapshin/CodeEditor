﻿using CodeEditor.Logic;
using CodeEditor.Logic.Models;
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeEditor
{
    public partial class Editor : Form
    {
        private CurrentOperation currentOperation;
        private string filePath = null;
        private CaretPosition caretPosition = null;
        private bool fileChanged = false;

        public Editor()
        {
            InitializeComponent();
            CreateRightClickMenu();
        }

        public void CreateRightClickMenu()
        {
            ContextMenu cm = new ContextMenu();
            MenuItem menuItemCreateConstant = new MenuItem("Create constant", MenuItemCreateConstant_Click, Shortcut.CtrlShiftK);
            MenuItem menuItemDecompose = new MenuItem("Decompose", MenuItemDecompose_Click, Shortcut.CtrlShiftD);
            MenuItem menuItemExtractMethod = new MenuItem("Extract method", MenuItemExtractMethod_Click, Shortcut.CtrlShiftE);
            MenuItem menuItemRenameMethod = new MenuItem("Rename method", MenuItemRenameMethod_Click, Shortcut.CtrlShiftR);
            MenuItem menuItemRenameVariable = new MenuItem("Rename variable", MenuItemRenameVariable_Click, Shortcut.CtrlShiftV);
            cm.MenuItems.AddRange(new MenuItem[] { menuItemCreateConstant, menuItemRenameVariable, menuItemDecompose, menuItemExtractMethod, menuItemRenameMethod });
            CodeEditor.ContextMenu = cm;
        }

        private void MenuItemExtractMethod_Click(object sender, System.EventArgs e)
        {
            ShowRefactoringMenu("Extract Method");
            currentOperation = CurrentOperation.ExtractMethod;
        }

        private void MenuItemRenameMethod_Click(object sender, System.EventArgs e)
        {
            ShowRefactoringMenu("Rename Method");
            currentOperation = CurrentOperation.RenameMethod;
        }

        private void MenuItemCreateConstant_Click(object sender, System.EventArgs e)
        {
            ShowRefactoringMenu("Create Constant");
            currentOperation = CurrentOperation.CreateConstant;
        }

        private void MenuItemDecompose_Click(object sender, System.EventArgs e)
        {
            ShowRefactoringMenu("Decompose");
            currentOperation = CurrentOperation.Decompose;
        }

        private void MenuItemRenameVariable_Click(object sender, System.EventArgs e)
        {
            ShowRefactoringMenu("Rename Variable");
            currentOperation = CurrentOperation.RenameVariable;
        }

        private void exitToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "XLine Editor", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            return;
                        break;
                    case DialogResult.No:
                        break;
                    case DialogResult.Cancel:
                        return;
                }
            }
            this.Close();
        }

        private void CodeEditor_TextChanged(object sender, System.EventArgs e)
        {
            fileChanged = true;
        }

        private void CodeEditor_SelectionChanged(object sender, System.EventArgs e)
        {
            caretPosition = CodeEditorHelper.GetCaretPosition(CodeEditor.Text, CodeEditor.SelectionStart);
            label1.Text = $"Line: {caretPosition.Row}, Column: {caretPosition.Column}";
        }

        private void openFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            filePath = openFileDialog1.FileName;
            string fileText = System.IO.File.ReadAllText(filePath);
            this.Text = $"XLine Editor {filePath.Substring(filePath.LastIndexOf('\\') + 1)}";
            CodeEditor.Text = fileText;
        }

        private void saveFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {

            if (SaveFile())
                fileChanged = false;
        }

        private void saveAsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            try
            {
                filePath = saveFileDialog1.FileName;
                System.IO.File.WriteAllText(filePath, CodeEditor.Text);
                this.Text = $"XLine Editor {filePath.Substring(filePath.LastIndexOf('\\') + 1)}";
                fileChanged = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show($"File is not saved. Error: {exc.Message}");
            }

            ShowSavedLabel();
        }


        private void newFIleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "XLine Editor", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            return;
                        break;
                    case DialogResult.No:
                        break;
                    case DialogResult.Cancel:
                        return;
                }
            }

            CodeEditor.Text = "";
            fileChanged = false;
        }

        private bool SaveFile()
        {
            if (filePath == null)
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return false;

                filePath = saveFileDialog1.FileName;
            }

            try
            {
                System.IO.File.WriteAllText(filePath, CodeEditor.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show($"File is not saved. Error: {e.Message}");
                return false;
            }

            ShowSavedLabel();

            return true;
        }

        private async Task ShowSavedLabel()
        {
            labelSaved.Visible = true;
            labelSaved.ForeColor = Color.FromArgb(64, 64, 64);

            for (int i = 64; i < 171; ++i)
            {
                labelSaved.ForeColor = Color.FromArgb(i, i, i);
                await Task.Delay(20);
            }
            labelSaved.Visible = false;

        }

        private void ShowRefactoringMenu(string operationName)
        {
            textBoxNewname.Text = "";
            panelRefactoringMenu.Visible = true;
            labelOperation.Text = operationName;
            textBoxSelection.Text = CodeEditor.SelectedText;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            panelRefactoringMenu.Visible = false;
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {

            if (textBoxNewname.Text.Length <= 0)
            {
                MessageBox.Show("Please enter new name", "XLine Editor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int selectionStart = CodeEditor.SelectionStart;

            switch (currentOperation)
            {
                case CurrentOperation.CreateConstant:
                    CodeEditor.Text = CodeRefactoring.CreateConstant(CodeEditor.Text, CodeEditor.SelectionStart, CodeEditor.SelectionStart + CodeEditor.SelectionLength, textBoxNewname.Text);
                    break;
                case CurrentOperation.Decompose:
                    CodeEditor.Text = CodeRefactoring.Decompose(CodeEditor.Text, caretPosition.Row, textBoxNewname.Text);
                    break;
                case CurrentOperation.ExtractMethod:
                    CodeEditor.Text = CodeRefactoring.ExtractMethod(CodeEditor.Text, CodeEditor.SelectionStart, CodeEditor.SelectionStart + CodeEditor.SelectionLength, textBoxNewname.Text);
                    break;
                case CurrentOperation.RenameMethod:
                    CodeEditor.Text = CodeRefactoring.RenameMethod(CodeEditor.Text, textBoxNewname.Text, CodeEditor.SelectionStart, CodeEditor.SelectionStart + CodeEditor.SelectionLength);
                    break;
                case CurrentOperation.RenameVariable:
                    CodeEditor.Text = CodeRefactoring.RenameVariable(CodeEditor.Text, CodeEditor.SelectedText, textBoxNewname.Text);
                    break;
            }

            CodeEditor.SelectionStart = selectionStart;

            panelRefactoringMenu.Visible = false;
        }

        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fileChanged)
            {
                var dialogResult = MessageBox.Show("Save changes?", "XLine Editor", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                switch (dialogResult)
                {
                    case DialogResult.Yes:
                        if (!SaveFile())
                            e.Cancel = true;
                        break;
                }
            }
        }
    }
}
