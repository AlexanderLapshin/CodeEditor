﻿using CodeEditor.Logic;
using FluentAssertions;
using Xunit;

namespace CodeEditor.Tests
{
    public class ExtractMethodTests
    {
        [Fact]
        public void CanExtractMethod()
        {
            string testFile = "string test = GetRandomString();";

            var result = CodeRefactoring.ExtractMethod(testFile, 0, 32, "NewMethod");

            var expected = "NewMethod();\nvoid NewMethod()\n{\nstring test = GetRandomString();\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractMethodWithVariableAfter()
        {
            string testFile = "string test = GetRandomString();\nstring str = test+\"cake\";";

            var result = CodeRefactoring.ExtractMethod(testFile, 0, 32, "NewMethod");

            var expected = "NewMethod();\nstring str = test+\"cake\";\nvoid NewMethod()\n{\nstring test = GetRandomString();\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractLineInMethod()
        {
            string testFile = "void Calculate()\n{\nint area = 7*8;\n}\nCalculate();";

            var result = CodeRefactoring.ExtractMethod(testFile, 19, 34, "NewMethod");

            var expected = "void Calculate()\n{\nNewMethod();\n}\nCalculate();\nvoid NewMethod()\n{\nint area = 7*8;\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractMultiLinesInMethod()
        {
            string testFile = "void Calculate()\n{\nint area = 7*8;\ncout<<\"area\"<<endl;\n}\nCalculate();";

            var result = CodeRefactoring.ExtractMethod(testFile, 19, 54, "NewMethod");

            var expected = "void Calculate()\n{\nNewMethod();\n}\nCalculate();\nvoid NewMethod()\n{\nint area = 7*8;\ncout<<\"area\"<<endl;\n}";

            result.Should().Be(expected);
        }
        [Fact]
        public void CycleExtractMethod()
        {
            string testFile = "for(int = 0; i<88 i++;)\n{\ncout<<\"area\"<<endl;\n}";

            var result = CodeRefactoring.ExtractMethod(testFile, 0, 47, "NewMethod");

            var expected = "NewMethod();\nvoid NewMethod()\n{\nfor(int = 0; i<88 i++;)\n{\ncout<<\"area\"<<endl;\n}\n}";
            result.Should().Be(expected);
        }
    }
}
