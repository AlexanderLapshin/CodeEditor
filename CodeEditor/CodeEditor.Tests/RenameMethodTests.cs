﻿using CodeEditor.Logic;
using FluentAssertions;
using Xunit;

namespace CodeEditor.Tests
{
    public class RenameMethodTests
    {
        [Fact]
        public void CanRenameMethod()
        {
            string TestFile = "string test = GetRandomString();";
            string Change = "RenameMethod";
            int selectionStart = 14;
            int selectionEnd = 29;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "string test = RenameMethod();";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMethodAfterMethod()
        {
            string TestFile = "string test = GetRandomString();";
            string Change = "RenameMethod";
            int selectionStart = 14;
            int selectionEnd = 29;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "string test = RenameMethod();";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMethodAfterVariable()
        {
            string TestFile = "int GetRandomString = 1488;\n" + "string test = GetRandomString();";
            string Change = "RenameMethod";
            int selectionStart = 42;
            int selectionEnd = 57;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "int GetRandomString = 1488;\n" + "string test = RenameMethod();";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMultipleMethods()
        {
            string TestFile = "string test = GetRandomString();\n" + "string test2 = GetRandomString();\n" + "string test3 = GetRandomString();";
            string Change = "RenameMethod";
            int selectionStart = 48;
            int selectionEnd = 63;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "string test = RenameMethod();\n" + "string test2 = RenameMethod();\n" + "string test3 = RenameMethod();";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMethodWithParams()
        {
            string TestFile = "string test = GetRandomString(10, 4);";
            string Change = "RenameMethod";
            int selectionStart = 14;
            int selectionEnd = 29;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "string test = RenameMethod(10, 4);";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMethodWithDeclaration()
        {
            string TestFile = "public string GetRandomString(string test1, string test2);";
            string Change = "RenameMethod";
            int selectionStart = 14;
            int selectionEnd = 29;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "public string RenameMethod(string test1, string test2);";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanRenameMethodWithDeclarationAndUsing()
        {
            string TestFile = "public string GetRandomString(string test1, string test2){};\npublic void Test {GetRandomString(\"test1\", \"test2\")";
            string Change = "RenameMethod";
            int selectionStart = 14;
            int selectionEnd = 29;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "public string RenameMethod(string test1, string test2){};\npublic void Test {RenameMethod(\"test1\", \"test2\")";
            result.Should().Be(expected);
        }
        [Fact]
        public void NothingChanged()
        {
            string TestFile = "public string GetRandomString(int test, int test1);";
            string Change = "RenameMethod";
            int selectionStart = 23;
            int selectionEnd = 32;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            result.Should().Be(TestFile);
        }
        [Fact]
        public void CanNotRenameMathExpression()
        {
            string TestFile = "string GetRandomString\nGetRandomString += (\"something\");";
            string Change = "RenameMethod";
            int selectionStart = 24;
            int selectionEnd = 39;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "string GetRandomString\nGetRandomString += (\"something\");";
            result.Should().Be(expected);
        }
        [Fact]
        public void CanNotRenameCycle()
        {
            string TestFile = "for(int i=0;i<9;i++)";
            string Change = "RenameMethod";
            int selectionStart = 0;
            int selectionEnd = 3;
            var result = CodeRefactoring.RenameMethod(TestFile, Change, selectionStart, selectionEnd);
            var expected = "for(int i=0;i<9;i++)";
            result.Should().Be(expected);
        }
    }
}
