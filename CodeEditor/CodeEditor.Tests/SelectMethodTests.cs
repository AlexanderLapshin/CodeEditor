﻿using CodeEditor.Logic;
using FluentAssertions;
using System;
using Xunit;

namespace CodeEditor.Tests
{
    public class SelectMethodTests
    {
        [Fact]
        public void CanExtractMethod()
        {
            string testFile = "string test = GetRandomString();";

            var result = CodeRefactoring.ExtractMetbod(testFile, 0, 33);

            var expected = "private static void NewMethod()\n{\nstring test = GetRandomString();\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractMethodWithMethodAfter()
        {
            string testFile = "string test = GetRandomString().ParseInt();\nstring test = GetRandomString();";

            var result = CodeRefactoring.ExtractMetbod(testFile, 0, 78);

            var expected = "private static void NewMethod()\n{\nstring test = GetRandomString().ParseInt();\n}\nstring test = GetRandomString();";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractLineInMethod1()
        {
            string testFile = "string test = GetRandomString().ParseInt();\nstring test = GetRandomString();";

            var result = CodeRefactoring.ExtractMetbod(testFile, 0, 46);

            var expected = "private static void NewMethod()\n{\nstring test = GetRandomString().ParseInt();\n}\nstring test = GetRandomString();\n";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanExtractLineInMethod2()
        {
            string testFile = "string test = GetRandomString().ParseInt();\nstring test = GetRandomString();";

            var result = CodeRefactoring.SelectMethod(testFile, 24);

            var expected = "private void Calculate()\n{\n NewMethod();}\nprivate static void NewMethod()\n{\nint area = 7*8;;\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CanSelectMethodWithDeclaration()
        {
            string testFile = "public string GetRandomString(int test, int test1);";

            var result = CodeRefactoring.SelectMethod(testFile, 24);

            var expected = "private void Calculate()\n{\nNewMethod();}\nprivate static void NewMethod()\n{\nint area = 7*8;\nConsole.WriteLine(area)\n}";

            result.Should().Be(expected);
        }

        [Fact]
        public void CantSelectMethod()
        {
            string testFile = "public string GetRandomString(int test, int test1);";

            Action result = () => CodeRefactoring.SelectMethod(testFile, 11);

            var expected = "private void Calculate()\n{\nint radius = 33;\n int area = 3.14*radius*radius;\nConsole.WriteLine(area)\n}";
        }

        [Fact]
        public void CantExctractMethod2()
        {
            string testFile = "private void Calculate()\n{\nint radius = 33;\n int area = 3.14*radius*radius;\n}";

            var result = CodeRefactoring.SelectMethod(testFile, 0, 10);

            var expected = "private void Calculate()\n{\nint radius = 33;\n int area = 3.14*radius*radius;\n}";
        }
        [Fact]
        public void CantExctractMethod3()
        {
            string testFile = "private void Calculate()\n{\nint radius = 33;\n int area = 3.14*radius*radius;\n}";

            var result = CodeRefactoring.SelectMethod(testFile, 48, 62);

            var expected = "private void Calculate()\n{\nint radius = 33;\n int area = 3.14*radius*radius;\n}";
        }
        [Fact]
        public void CycleExtractMethod()
        {
            string testFile = "for( int = 0; i++; i<88){\nConsole.WriteLine(\"hello\");\n}";
            
            var result = CodeRefactoring.SelectMethod(testFile, 27, 53);

            var expected = "for( int = 0; i++; i<88){\nNewMethod();\n}\nprivate static void NewMethod(){\nConsole.WriteLine(\"hello\");\n}";
        }
    }
}
