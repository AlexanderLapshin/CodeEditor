﻿namespace CodeEditor.Logic.Models
{
    public enum CurrentOperation
    {
        CreateConstant,
        ExtractMethod,
        RenameMethod,
        Decompose,
        RenameVariable
    }
}
