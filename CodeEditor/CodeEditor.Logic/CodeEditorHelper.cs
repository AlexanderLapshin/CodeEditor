﻿using CodeEditor.Logic.Models;

namespace CodeEditor.Logic
{
    public static class CodeEditorHelper
    {
        public static CaretPosition GetCaretPosition(string file, int selectionStart)
        {
            int row = 1;
            int column = 1;
            for (int i = 0; i < selectionStart; i++)
            {
                column++;
                if (file[i] == '\n')
                {
                    row++;
                    column = 1;
                }
            }

            return new CaretPosition { Column = column, Row = row };
        }
    }
}
